package fit5042.tutex.calculator;

import java.util.ArrayList;
import javax.ejb.Stateful;


import fit5042.tutex.calculator.CompareProperty;
import fit5042.tutex.repository.entities.Property;

@Stateful
public class ComparePropertySessionBean implements CompareProperty {

	private ArrayList<Property> propertyList = new ArrayList<>();
	
	@Override
	public void addProperty(Property property) {
		propertyList.add(property);
	}
	
	@Override
	public void removeProperty(Property property) {
		propertyList.remove(property);
	}
	
	@Override
	public int bestPerRoom() {
		int bestIndex = 0; //initialize the bext index
		double bestPerRoomPrice = 99999999; // set the bestprice as a big number
		for (Property p:propertyList) {
			if (p.getPrice() < bestPerRoomPrice) {
				bestPerRoomPrice = p.getPrice();
				bestIndex = p.getPropertyId();
			}
		}
		return bestIndex;
	}
}
